import logo from "./logo.svg";
import "./App.css";
import { useState } from "react";
import { EditModel } from "./EditModal";

const cars = [
  {
    carName: "BMW",
    model: "320i",
    id: 1,
  },
  {
    carName: "Mercedes",
    model: "c63",
    id: 2,
  },
  {
    carName: "Tesla",
    model: "model x",
    id: 3,
  },
];

function App() {
  const [allCars, setAllCars] = useState(cars);
  const [newCar, setNewCar] = useState("");
  const [newId, setNewId] = useState(0);

  const [parseId, setParseId] = useState(0)

  const [openModal, setOpenModal] = useState(false)

  function handleAddCar() {
    const carToAdd = {
      carName: newCar,
      id: newId
    }
    setAllCars((cur) => [...cur, carToAdd])
  }

  function handleDelete(id) {
    const filtered = allCars.filter((car) => car.id !== id)
    setAllCars(filtered)
  }

  function handleEdit(id, carToEdit) {
    const edited = allCars.map(car => car.id === id ? {...carToEdit} : car)
    setAllCars(edited)
    setOpenModal(false)
  }

  function handleOpenEdit(id) {
    setParseId(id)
    setOpenModal(true)
  }


  return (
    <div className="App">
      <h1>Learn Update</h1>

      <div>
        <input
          placeholder="car name"
          type="text"
          onChange={(e) => setNewCar(e.target.value)}
        />
        <input
          placeholder="id"
          type="number"
          onChange={(e) => setNewId(e.target.value)}
        />
        <button onClick={handleAddCar}>Add Car</button>
      </div>

      <div>
        {allCars.map((car) => (
          <div>
            <span><b>{car.carName} </b></span>
            <span>{car.model}</span>
            <button onClick={() => handleDelete(car.id)}>Delete</button>
            <button onClick={() => handleOpenEdit(car.id)}>Open Edit</button>
            {/* <input type="text" placeholder="Edit Car" onChange={(e) => setEditCarName(e.target.value)}/>
            <input type="text" placeholder="Edit Model" onChange={(e) => setEditCarModel(e.target.value)}/>
            <button onClick={() => handleEdit(car.id)}>Edit</button> */}
          </div>
        ))}
      </div>

      {openModal ? <EditModel parseId={parseId} handleEdit={handleEdit} /> : null}
    </div>
  );
}

export default App;
