import { useState } from "react";
export function EditModel({ parseId, handleEdit }) {

    const [modalCarName, setModalCarName] = useState("")
    const [modalCarModel, setModalCarModel] = useState("")

    const obj = {
        carName: modalCarName,
        model: modalCarModel
    }
  
  return (
    <div>
      <p>{parseId}</p>
      <input type="text" placeholder="edit car name" onChange={(e) => setModalCarName(e.target.value)} />
      <input type="text" placeholder="edit car model" onChange={(e) => setModalCarModel(e.target.value)} />
      <button onClick={() => handleEdit(parseId, obj)}>submit</button>
    </div>
  );
}
